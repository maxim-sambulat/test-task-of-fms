package com.maximsambulat.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@Import({
    DatabaseConfiguration.class,
    WebMvcConfiguration.class,
    WebSecurityConfiguration.class,
    ServiceConfiguration.class,
    ValidationConfiguration.class,
    ConverterConfiguration.class,
    ErrorHandlerConfiguration.class
})
@PropertySource({
    "classpath:properties/application.properties",
    "classpath:properties/datasource.properties"
})
public class ApplicationConfiguration {
}
