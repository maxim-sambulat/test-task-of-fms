package com.maximsambulat.configuration;

import com.maximsambulat.converter.NewRoleDtoToRoleEntity;
import com.maximsambulat.converter.NewUserDtoToUserEntity;
import com.maximsambulat.converter.RoleEntityToRoleResource;
import com.maximsambulat.converter.UserEntityToUserResource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.converter.Converter;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
public class ConverterConfiguration {
    @Bean
    public ConversionServiceFactoryBean conversionService() {
        final ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
        final Set<Converter> converters = new HashSet<>();
        converters.add(new NewRoleDtoToRoleEntity());
        converters.add(new NewUserDtoToUserEntity());
        converters.add(new RoleEntityToRoleResource());
        converters.add(new UserEntityToUserResource());
        bean.setConverters(converters);
        return bean;
    }
}
