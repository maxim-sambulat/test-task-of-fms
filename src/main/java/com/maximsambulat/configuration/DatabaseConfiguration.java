package com.maximsambulat.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.sql.DataSource;
import java.util.Properties;

import static java.lang.Integer.parseInt;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@ComponentScan(basePackages = "com.maximsambulat.dao")
@EnableTransactionManagement
public class DatabaseConfiguration implements TransactionManagementConfigurer {
    private static final String POOL_NAME = "springHikariCP";
    private static final String HIBERNATE_HBM2DDL_AUTO = "validate";

    private final Environment environment;

    @Autowired
    public DatabaseConfiguration(final Environment environment) {
        this.environment = environment;
    }

    @Bean
    public DataSource dataSource() {
        return new HikariDataSource(
            getHikariConfig()
        );
    }

    private HikariConfig getHikariConfig() {
        final HikariConfig config = new HikariConfig();
        config.setPoolName(POOL_NAME);
        config.setDriverClassName(environment.getProperty("db.driver"));
        config.setJdbcUrl(environment.getProperty("db.url"));
        config.setUsername(environment.getProperty("db.username"));
        config.setPassword(environment.getProperty("db.password"));
        config.setMaximumPoolSize(parseInt(environment.getProperty("db.pool.size")));
        config.setIdleTimeout(Long.parseLong(environment.getProperty("db.hikari.idleConnectionTimeout")));
        config.setLeakDetectionThreshold(Long.parseLong(environment.getProperty("db.hikari.leakDetectionThreshold")));
        config.addDataSourceProperty(
            "cachePrepStmts",
            environment.getProperty("db.hikari.cachePrepStmts")
        );
        config.addDataSourceProperty(
            "prepStmtCacheSize",
            environment.getProperty("db.hikari.prepStmtCacheSize")
        );
        config.addDataSourceProperty(
            "prepStmtCacheSqlLimit",
            environment.getProperty("db.hikari.prepStmtCacheSqlLimit")
        );
        config.addDataSourceProperty(
            "useServerPrepStmts",
            environment.getProperty("db.hikari.useServerPrepStmts")
        );
        return config;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManager() {
        final LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource());
        emf.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        emf.setPackagesToScan("com.maximsambulat.domain.entity");
        emf.setJpaProperties(getHibernateProperties());
        return emf;
    }

    private Properties getHibernateProperties() {
        final Properties properties = new Properties();
        properties.put(
            "hibernate.dialect",
            environment.getProperty("db.hibernate.dialect")
        );
        properties.put(
            "hibernate.show_sql",
            environment.getProperty("db.hibernate.show_sql")
        );
        properties.setProperty(
            "hibernate.max_fetch_depth",
            environment.getProperty("db.fetch_depth")
        );
        properties.setProperty(
            "hibernate.jdbc.fetch_size",
            environment.getProperty("db.fetch_size")
        );
        properties.setProperty(
            "hibernate.jdbc.batch_size",
            environment.getProperty("db.batch_size")
        );
        properties.put(
            "hibernate.hbm2ddl.auto",
            environment.getProperty("db.hibernate.hbm2ddl.auto", HIBERNATE_HBM2DDL_AUTO)
        );
        return properties;
    }

    @Bean
    public PlatformTransactionManager txManager() {
        return new JpaTransactionManager();
    }

    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return txManager();
    }
}
