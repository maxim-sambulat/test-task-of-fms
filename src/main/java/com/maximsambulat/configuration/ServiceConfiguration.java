package com.maximsambulat.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@ComponentScan(basePackages = "com.maximsambulat.service")
public class ServiceConfiguration {
}
