package com.maximsambulat.configuration;

import com.maximsambulat.validator.ValidationService;
import com.maximsambulat.validator.ValidationServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class ValidationConfiguration {
    @Bean
    public LocalValidatorFactoryBean validatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public ValidationService validationService() {
        return new ValidationServiceImpl(validatorFactoryBean().getValidator());
    }
}
