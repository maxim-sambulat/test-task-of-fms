package com.maximsambulat.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.maximsambulat.controller")
public class WebMvcConfiguration extends WebMvcConfigurationSupport {

}
