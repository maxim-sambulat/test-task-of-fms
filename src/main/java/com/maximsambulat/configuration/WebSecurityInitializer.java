package com.maximsambulat.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Maxim Sambulat.
 */
public class WebSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
