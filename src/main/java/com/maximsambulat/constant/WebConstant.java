package com.maximsambulat.constant;

/**
 * Created by Maxim Sambulat.
 */
public interface WebConstant {
    String API_PREFIX = "/api";
    String AUTHENTICATE_HEADER_NAME = "Authenticate-Token";
    String QUERY_PARAMETER_USERNAME = "username";
    String QUERY_PARAMETER_PASSWORD = "password";
}
