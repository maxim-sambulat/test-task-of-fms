package com.maximsambulat.controller;

import com.maximsambulat.security.SecurityTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.maximsambulat.constant.WebConstant.AUTHENTICATE_HEADER_NAME;
import static com.maximsambulat.constant.WebConstant.QUERY_PARAMETER_PASSWORD;
import static com.maximsambulat.constant.WebConstant.QUERY_PARAMETER_USERNAME;
import static com.maximsambulat.controller.AuthenticationController.AUTHENTICATION_CONTROLLER_URL;

/**
 * Created by Maxim Sambulat.
 */
@RestController
@RequestMapping(AUTHENTICATION_CONTROLLER_URL)
public class AuthenticationController {
    public static final String AUTHENTICATION_CONTROLLER_URL = "/login";
    private final UserDetailsService userDetailsService;
    private final SecurityTokenService tokenService;

    @Autowired
    public AuthenticationController(final UserDetailsService userDetailsService,
                                    final SecurityTokenService tokenService) {
        this.userDetailsService = userDetailsService;
        this.tokenService = tokenService;
    }

    @RequestMapping(method = {RequestMethod.POST})
    public ResponseEntity<Void> login(@RequestParam(QUERY_PARAMETER_USERNAME) final String username,
                                      @RequestParam(QUERY_PARAMETER_PASSWORD) final String password) {
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        if (userDetails.getPassword().equals(password)) {
            HttpHeaders headers = new HttpHeaders();
            headers.add(AUTHENTICATE_HEADER_NAME, tokenService.getJwtToken(username));
            return (new ResponseEntity<>(headers, HttpStatus.OK));
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
