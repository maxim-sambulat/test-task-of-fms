package com.maximsambulat.controller;

import com.maximsambulat.domain.dto.NewRoleDto;
import com.maximsambulat.domain.dto.UpdateRoleDto;
import com.maximsambulat.domain.resource.RoleResource;
import com.maximsambulat.service.role.RoleService;
import com.maximsambulat.validator.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static com.maximsambulat.constant.WebConstant.API_PREFIX;
import static com.maximsambulat.controller.RoleController.ROLE_CONTROLLER_URL;

/**
 * Created by Maxim Sambulat.
 */
@RestController
@RequestMapping(ROLE_CONTROLLER_URL)
public class RoleController {
    public static final String ROLE_CONTROLLER_URL = API_PREFIX + "/roles";
    private final ValidationService validationService;
    private final RoleService roleService;

    @Autowired
    public RoleController(final ValidationService validationService,
                          final RoleService roleService) {
        this.validationService = validationService;
        this.roleService = roleService;
    }

    @RequestMapping(method = {RequestMethod.POST})
    public ResponseEntity<Void> create(@RequestBody final NewRoleDto role) {
        validationService.validate(role);
        final RoleResource result = roleService.create(role);
        final URI location = ServletUriComponentsBuilder
            .fromCurrentRequest().path("/{id}")
            .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @RequestMapping(method = {RequestMethod.GET})
    public ResponseEntity<List<RoleResource>> getAll() {
        final List<RoleResource> result = roleService.getAll();
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{roleId}", method = {RequestMethod.GET})
    public ResponseEntity<RoleResource> getById(@PathVariable("roleId") final Long roleId) {
        final RoleResource result = roleService.getById(roleId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{roleId}", method = {RequestMethod.PUT})
    public ResponseEntity<RoleResource> update(@PathVariable("roleId") final Long roleId,
                                               @RequestBody final UpdateRoleDto updateRoleDto) {
        validationService.validate(updateRoleDto);
        final RoleResource result = roleService.update(roleId, updateRoleDto);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{roleId}", method = {RequestMethod.DELETE})
    public ResponseEntity<Void> delete(@PathVariable("roleId") final Long roleId) {
        roleService.delete(roleId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
