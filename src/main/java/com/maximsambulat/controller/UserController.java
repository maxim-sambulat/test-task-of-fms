package com.maximsambulat.controller;

import com.maximsambulat.domain.dto.AppendRoleDto;
import com.maximsambulat.domain.dto.NewUserDto;
import com.maximsambulat.domain.dto.UpdateUserDto;
import com.maximsambulat.domain.resource.RoleResource;
import com.maximsambulat.domain.resource.UserResource;
import com.maximsambulat.service.user.UserService;
import com.maximsambulat.validator.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static com.maximsambulat.constant.WebConstant.API_PREFIX;
import static com.maximsambulat.controller.UserController.USER_CONTROLLER_URL;

/**
 * Created by Maxim Sambulat.
 */
@RestController
@RequestMapping(USER_CONTROLLER_URL)
public class UserController {
    public static final String USER_CONTROLLER_URL = API_PREFIX + "/users";
    private final ValidationService validationService;
    private final UserService userService;

    @Autowired
    public UserController(final ValidationService validationService,
                          final UserService userService) {
        this.validationService = validationService;
        this.userService = userService;
    }

    @RequestMapping(method = {RequestMethod.POST})
    public ResponseEntity<Void> create(@RequestBody final NewUserDto user) {
        validationService.validate(user);
        final UserResource result = userService.create(user);
        final URI location = ServletUriComponentsBuilder
            .fromCurrentRequest().path("/{id}")
            .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @RequestMapping(method = {RequestMethod.GET})
    public ResponseEntity<List<UserResource>> getAll() {
        final List<UserResource> result = userService.getAll();
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{clientId}", method = {RequestMethod.GET})
    public ResponseEntity<UserResource> getById(@PathVariable("clientId") final Long clientId) {
        final UserResource result = userService.getById(clientId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{clientId}", method = {RequestMethod.PUT})
    public ResponseEntity<UserResource> update(@PathVariable("clientId") final Long clientId,
                                               @RequestBody final UpdateUserDto updateClientDto) {
        validationService.validate(updateClientDto);
        final UserResource result = userService.update(clientId, updateClientDto);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{clientId}", method = {RequestMethod.DELETE})
    public ResponseEntity<Void> delete(@PathVariable("clientId") final Long clientId) {
        userService.delete(clientId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{clientId}/roles", method = {RequestMethod.PUT})
    public ResponseEntity<Void> addRole(@PathVariable("clientId") final Long clientId,
                                        @RequestBody final AppendRoleDto role) {
        userService.addRole(clientId, role.getId());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{clientId}/roles", method = {RequestMethod.GET})
    public ResponseEntity<List<RoleResource>> getAllRoles(@PathVariable("clientId") final Long clientId) {
        final List<RoleResource> result = userService.getAllRoles(clientId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{clientId}/roles/{roleId}", method = {RequestMethod.DELETE})
    public ResponseEntity<Void> deleteRole(@PathVariable("clientId") final Long clientId,
                                           @PathVariable("roleId") final Long roleId) {
        userService.deleteRole(clientId, roleId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
