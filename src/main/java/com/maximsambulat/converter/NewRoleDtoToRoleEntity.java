package com.maximsambulat.converter;

import com.maximsambulat.domain.dto.NewRoleDto;
import com.maximsambulat.domain.entity.RoleEntity;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by Maxim Sambulat.
 */
public class NewRoleDtoToRoleEntity implements Converter<NewRoleDto, RoleEntity> {
    @Override
    public RoleEntity convert(final NewRoleDto source) {
        final RoleEntity entity = new RoleEntity();
        entity.setName(source.getName());
        return entity;
    }
}
