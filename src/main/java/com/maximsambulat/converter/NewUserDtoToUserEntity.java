package com.maximsambulat.converter;

import com.maximsambulat.domain.dto.NewUserDto;
import com.maximsambulat.domain.entity.UserEntity;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by Maxim Sambulat.
 */
public class NewUserDtoToUserEntity implements Converter<NewUserDto, UserEntity> {
    @Override
    public UserEntity convert(NewUserDto source) {
        final UserEntity entity = new UserEntity();
        entity.setActive(source.getActive());
        entity.setUsername(source.getUsername());
        entity.setPassword(source.getPassword());
        return entity;
    }
}
