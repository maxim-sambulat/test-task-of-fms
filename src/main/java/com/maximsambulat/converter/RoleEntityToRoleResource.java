package com.maximsambulat.converter;

import com.maximsambulat.domain.entity.RoleEntity;
import com.maximsambulat.domain.resource.RoleResource;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by Maxim Sambulat.
 */
public class RoleEntityToRoleResource implements Converter<RoleEntity, RoleResource> {
    @Override
    public RoleResource convert(RoleEntity source) {
        final RoleResource resource = new RoleResource();
        resource.setId(source.getId());
        resource.setName(source.getName());
        return resource;
    }
}
