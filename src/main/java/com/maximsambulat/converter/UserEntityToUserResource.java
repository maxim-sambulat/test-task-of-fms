package com.maximsambulat.converter;

import com.maximsambulat.domain.entity.UserEntity;
import com.maximsambulat.domain.resource.UserResource;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by Maxim Sambulat.
 */
public class UserEntityToUserResource implements Converter<UserEntity, UserResource> {
    @Override
    public UserResource convert(UserEntity source) {
        final UserResource resource = new UserResource();
        resource.setId(source.getId());
        resource.setActive(source.isActive());
        resource.setUsername(source.getUsername());
        resource.setPassword(source.getPassword());
        return resource;
    }
}
