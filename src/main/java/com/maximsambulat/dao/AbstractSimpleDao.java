package com.maximsambulat.dao;

import com.maximsambulat.domain.entity.Identifiable;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.isNull;

/**
 * Created by Maxim Sambulat.
 */
public abstract class AbstractSimpleDao<ID, T extends Identifiable<ID>>
    implements SimpleDao<T> {

    private final Class<T> classEntity;

    @PersistenceContext
    private EntityManager entityManager;

    public AbstractSimpleDao(final Class<T> classEntity) {
        this.classEntity = classEntity;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    @Transactional(readOnly = true)
    public T findOne(Long id) {
        Objects.requireNonNull(id);

        final T entity = entityManager.find(classEntity, id);
        if (isNull(entity)) {
            throw new EntityNotFoundException(
                String.format("User with id: '%s' not found.", id)
            );
        }

        return entity;
    }

    @Override
    @Transactional(readOnly = true)
    public List<T> findAll() {
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> query = builder.createQuery(classEntity);
        final Root<T> from = query.from(classEntity);
        query.select(from);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    @Transactional
    public T save(final T entity) {
        final T result;
        if (isNull(entity.getId())) {
            if (exist(entity)) {
                throw new EntityExistsException("Entity is exit: '" + entity.toString() + "'");
            }
            entityManager.persist(entity);
            result = entity;
        } else {
            result = entityManager.merge(entity);
        }
        return result;
    }

    @Override
    @Transactional
    public void delete(final Long id) {
        final T entity = findOne(id);
        entityManager.remove(entity);
    }

    @Override
    public void flush() {
        entityManager.flush();
    }

    @Transactional(readOnly = true)
    public boolean exist(final T entity) {
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Long> query = builder.createQuery(Long.class);
        final Root<T> from = query.from(classEntity);
        query.select(builder.count(from))
            .where(getExistPredicate(entity, builder, from));
        final Long count = entityManager.createQuery(query)
            .getSingleResult();
        return count > 0;
    }

    protected abstract Predicate getExistPredicate(final T entity,
                                                   final CriteriaBuilder builder,
                                                   final Root<T> from);
}
