package com.maximsambulat.dao;

import com.maximsambulat.domain.entity.RoleEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Maxim Sambulat.
 */
@Repository
public interface RoleDao extends SimpleDao<RoleEntity> {
}
