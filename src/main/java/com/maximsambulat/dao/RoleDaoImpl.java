package com.maximsambulat.dao;

import com.maximsambulat.domain.entity.RoleEntity;
import com.maximsambulat.domain.entity.RoleEntity_;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created by Maxim Sambulat.
 */
@Repository
public class RoleDaoImpl
    extends AbstractSimpleDao<Long, RoleEntity>
    implements RoleDao {

    public RoleDaoImpl() {
        super(RoleEntity.class);
    }

    @Override
    protected Predicate getExistPredicate(final RoleEntity entity,
                                          final CriteriaBuilder builder,
                                          final Root<RoleEntity> from) {
        return builder.equal(from.get(RoleEntity_.name), entity.getName());
    }
}
