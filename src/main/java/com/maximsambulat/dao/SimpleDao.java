package com.maximsambulat.dao;

import java.util.List;

/**
 * Created by Maxim Sambulat.
 */
public interface SimpleDao<T> {
    T findOne(Long id);

    List<T> findAll();

    T save(T entity);

    void delete(Long id);

    void flush();
}
