package com.maximsambulat.dao;

import com.maximsambulat.domain.entity.UserEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Maxim Sambulat.
 */
@Repository
public interface UserDao extends SimpleDao<UserEntity> {
}
