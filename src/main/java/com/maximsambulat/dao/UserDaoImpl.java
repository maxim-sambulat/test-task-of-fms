package com.maximsambulat.dao;

import com.maximsambulat.domain.entity.UserEntity;
import com.maximsambulat.domain.entity.UserEntity_;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created by Maxim Sambulat.
 */
@Repository
public class UserDaoImpl
    extends AbstractSimpleDao<Long, UserEntity>
    implements UserDao {

    public UserDaoImpl() {
        super(UserEntity.class);
    }

    @Override
    protected Predicate getExistPredicate(final UserEntity entity,
                                          final CriteriaBuilder builder,
                                          final Root<UserEntity> from) {
        return builder.equal(from.get(UserEntity_.username), entity.getUsername());
    }
}
