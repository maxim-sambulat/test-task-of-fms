package com.maximsambulat.domain.entity;

/**
 * Created by Maxim Sambulat.
 */
public interface Identifiable<ID> {
    ID getId();
}
