package com.maximsambulat.domain.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Maxim Sambulat.
 */

@Entity
@Table(name = "role")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"name"})
@EqualsAndHashCode(of = {"name"})
public class RoleEntity implements Serializable, Identifiable<Long> {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
}
