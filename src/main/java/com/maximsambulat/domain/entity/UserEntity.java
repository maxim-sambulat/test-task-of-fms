package com.maximsambulat.domain.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Maxim Sambulat.
 */
@Entity
@Table(name = "user")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(of = {"username"})
@EqualsAndHashCode(of = {"username"})
public class UserEntity implements Serializable, Identifiable<Long> {
    @Id
    @GeneratedValue
    private Long id;
    private boolean active;
    private String username;
    private String password;
    @ManyToMany(cascade = CascadeType.REMOVE)
    private Set<RoleEntity> roles;
}
