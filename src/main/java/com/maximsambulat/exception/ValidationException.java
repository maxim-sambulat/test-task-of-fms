package com.maximsambulat.exception;

import com.maximsambulat.validator.Violation;
import lombok.Getter;

import java.util.List;

@Getter
public class ValidationException extends RuntimeException {
    private final List<Violation> violations;

    public ValidationException(final List<Violation> violations, final String message) {
        super(message);
        this.violations = violations;
    }
}
