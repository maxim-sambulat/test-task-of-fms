package com.maximsambulat.handler;

import lombok.Getter;

@Getter
public class FailureEntity<T> {
    private final String message;
    private final T content;

    public FailureEntity(final String message, final T content) {
        this.message = message;
        this.content = content;
    }
}
