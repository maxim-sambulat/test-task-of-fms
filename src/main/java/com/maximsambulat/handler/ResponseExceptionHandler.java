package com.maximsambulat.handler;

import com.maximsambulat.exception.ValidationException;
import com.maximsambulat.validator.Violation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.ResponseEntity.status;

@Slf4j
@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {
    private static final String NOT_FOUND = "Entity not found";
    private static final String VALIDATION_EXCEPTION = "Error of validation entity";
    private static final String INTERNAL_SERVER_ERROR = "Unknown server error";
    private static final String PERSISTENCE_EXCEPTION = "Persistence exception";
    private static final String ENTITY_EXISTS = "Entity exists";
    private static final String USERNAME_NOT_FOUND_EXCEPTION = "Username not found.";

    @ExceptionHandler(value = {EntityNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected void entityNotFoundException(final EntityNotFoundException e) {
        log.info(NOT_FOUND, e);
    }

    @ExceptionHandler(value = {EntityExistsException.class})
    @ResponseStatus(CONFLICT)
    protected void handleEntityExists(final EntityExistsException e) {
        log.info(ENTITY_EXISTS, e);
    }

    @ExceptionHandler(value = {UsernameNotFoundException.class})
    protected ResponseEntity<?> handleUsernameNotFoundException(UsernameNotFoundException e) {
        log.info(USERNAME_NOT_FOUND_EXCEPTION, e);
        return status(HttpStatus.NOT_FOUND).body(USERNAME_NOT_FOUND_EXCEPTION);
    }

    @ExceptionHandler(value = {ValidationException.class})
    protected ResponseEntity<FailureEntity<List<Violation>>> handleValidationException(final ValidationException e) {
        log.error(VALIDATION_EXCEPTION, e);
        return status(HttpStatus.BAD_REQUEST)
            .body(
                new FailureEntity<>(VALIDATION_EXCEPTION, e.getViolations())
            );
    }

    @ExceptionHandler(value = {JpaSystemException.class})
    protected ResponseEntity<?> handleJpaSystemException(final JpaSystemException e) {
        log.error(PERSISTENCE_EXCEPTION, e);
        return status(HttpStatus.INTERNAL_SERVER_ERROR).body(PERSISTENCE_EXCEPTION);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<?> handleInternalServerError(Exception e) {
        log.info(INTERNAL_SERVER_ERROR, e);
        return status(HttpStatus.INTERNAL_SERVER_ERROR).body(INTERNAL_SERVER_ERROR);
    }
}
