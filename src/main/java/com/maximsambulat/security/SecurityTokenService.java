package com.maximsambulat.security;

/**
 * Created by Maxim Sambulat.
 */
public interface SecurityTokenService {
    String getJwtToken(String username);
    Token getToken(String jwtToken);
}
