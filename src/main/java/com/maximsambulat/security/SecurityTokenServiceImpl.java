package com.maximsambulat.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationServiceException;

import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

/**
 * Created by Maxim Sambulat.
 */
public class SecurityTokenServiceImpl implements SecurityTokenService {
    private final Key secureKey;
    private final Environment environment;

    @Autowired
    public SecurityTokenServiceImpl(final Environment environment) {
        this.environment = environment;
        secureKey = MacProvider.generateKey();
    }

    @Override
    public String getJwtToken(final String username) {
        return Jwts.builder()
            .setSubject(username)
            .setExpiration(getExpirationDate())
            .signWith(SignatureAlgorithm.HS256, secureKey)
            .compact();
    }

    private Date getExpirationDate() {
        final int seconds = Integer.parseInt(environment.getProperty("token.expiry_date"));
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, seconds);
        return calendar.getTime();
    }

    @Override
    public Token getToken(final String jwtToken) {
        final Claims claims = getClaims(jwtToken);
        final String username = getUsername(claims);
        return new Token(username);
    }

    private Claims getClaims(final String jwtToken) {
        try {
            return Jwts.parser()
                .setSigningKey(secureKey)
                .parseClaimsJws(jwtToken)
                .getBody();
        } catch (Exception ex) {
            throw new AuthenticationServiceException("Token corrupted.");
        }
    }

    private String getUsername(final Claims claims) {
        return Optional.ofNullable(claims.getSubject())
            .orElseThrow(() ->
                new AuthenticationServiceException("Unknown user.")
            );
    }
}
