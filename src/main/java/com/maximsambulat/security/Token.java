package com.maximsambulat.security;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Maxim Sambulat.
 */
@Getter
@AllArgsConstructor
public class Token {
    private String username;
}
