package com.maximsambulat.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * Created by Maxim Sambulat.
 */
public class TokenAuthentication implements Authentication {
    private String token;
    private boolean authenticated;
    private UserDetails principal;

    public TokenAuthentication(final String token) {
        this.token = token;
    }

    public TokenAuthentication(final String token,
                               final boolean authenticated,
                               final UserDetails principal) {
        this.token = token;
        this.authenticated = authenticated;
        this.principal = principal;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return principal.getAuthorities();
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public String getName() {
        return principal != null
               ? principal.getUsername()
               : null;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(final boolean authenticated) {
        this.authenticated = authenticated;
    }

    public String getToken() {
        return token;
    }
}
