package com.maximsambulat.security;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static com.maximsambulat.constant.WebConstant.API_PREFIX;
import static com.maximsambulat.constant.WebConstant.AUTHENTICATE_HEADER_NAME;

/**
 * Created by Maxim Sambulat.
 */
public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    public TokenAuthenticationFilter() {
        super(API_PREFIX + "/**");
        setAuthenticationSuccessHandler(new TokenAuthenticationSuccessHandler());
    }

    @Override
    public Authentication attemptAuthentication(final HttpServletRequest request,
                                                final HttpServletResponse response)
        throws AuthenticationException, IOException, ServletException {

        final Authentication authentication =
            Optional.ofNullable(request.getHeader(AUTHENTICATE_HEADER_NAME))
                .map(this::authenticate)
                .orElseThrow(() ->
                    new BadCredentialsException("")
                );

        addAuthenticateHeader(response, authentication);

        return authentication;
    }

    private void addAuthenticateHeader(final HttpServletResponse response,
                                       final Authentication authentication) {
        if (authentication.isAuthenticated()
            && authentication instanceof TokenAuthentication) {
            response.setHeader(
                AUTHENTICATE_HEADER_NAME,
                ((TokenAuthentication) authentication).getToken()
            );
        }
    }

    private Authentication authenticate(final String token) {
        return getAuthenticationManager()
            .authenticate(
                new TokenAuthentication(token)
            );
    }
}
