package com.maximsambulat.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by Maxim Sambulat.
 */
public class TokenAuthenticationManager implements AuthenticationManager {
    private final UserDetailsService userDetailsService;
    private final SecurityTokenService tokenService;

    @Autowired
    public TokenAuthenticationManager(final UserDetailsService userDetailsService,
                                      final SecurityTokenService tokenService) {
        this.userDetailsService = userDetailsService;
        this.tokenService = tokenService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        if (authentication instanceof TokenAuthentication) {
            return processAuthentication((TokenAuthentication) authentication);
        } else {
            authentication.setAuthenticated(false);
            return authentication;
        }
    }

    private TokenAuthentication processAuthentication(final TokenAuthentication authentication) {
        final Token token = tokenService.getToken(authentication.getToken());
        final UserDetails user = userDetailsService.loadUserByUsername(token.getUsername());
        if (user.isEnabled()) {
            return new TokenAuthentication(
                tokenService.getJwtToken(token.getUsername()),
                true,
                user
            );
        } else {
            throw new DisabledException("User: '" + token.getUsername() + "' disabled.");
        }
    }
}
