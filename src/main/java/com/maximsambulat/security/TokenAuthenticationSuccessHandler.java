package com.maximsambulat.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Maxim Sambulat.
 */
public class TokenAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws ServletException, IOException {
        final String url = getUrl(request);
        request.getRequestDispatcher(url)
            .forward(request, response);
    }

    private String getUrl(HttpServletRequest request) {
        final StringBuilder url = new StringBuilder(request.getServletPath());
        final String pathInfo = request.getPathInfo();
        if (pathInfo != null) {
            url.append(pathInfo);
        }
        final String queryParams = request.getQueryString();
        if (queryParams != null) {
            url.append("?").append(queryParams);
        }
        return url.toString();
    }
}
