package com.maximsambulat.service.role;

import com.maximsambulat.domain.dto.NewRoleDto;
import com.maximsambulat.domain.dto.UpdateRoleDto;
import com.maximsambulat.domain.resource.RoleResource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Maxim Sambulat.
 */
@Service
public interface RoleService {
    RoleResource create(NewRoleDto newRole);

    List<RoleResource> getAll();

    RoleResource getById(Long roleId);

    RoleResource update(Long roleId, UpdateRoleDto updateRole);

    void delete(Long roleId);
}
