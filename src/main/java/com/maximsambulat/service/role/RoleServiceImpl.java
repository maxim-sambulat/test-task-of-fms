package com.maximsambulat.service.role;

import com.maximsambulat.dao.RoleDao;
import com.maximsambulat.domain.dto.NewRoleDto;
import com.maximsambulat.domain.dto.UpdateRoleDto;
import com.maximsambulat.domain.entity.RoleEntity;
import com.maximsambulat.domain.resource.RoleResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by Maxim Sambulat.
 */
@Service
public class RoleServiceImpl implements RoleService {
    private final RoleDao dao;
    private final ConversionService conversionService;

    @Autowired
    public RoleServiceImpl(final RoleDao dao,
                           final ConversionService conversionService) {
        this.dao = dao;
        this.conversionService = conversionService;
    }

    @Override
    @Transactional
    public RoleResource create(final NewRoleDto newRole) {
        final RoleEntity entity = toRoleEntity(newRole);
        dao.save(entity);
        return toRoleResource(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RoleResource> getAll() {
        return dao.findAll().stream()
            .map(this::toRoleResource)
            .collect(toList());
    }

    @Override
    @Transactional(readOnly = true)
    public RoleResource getById(final Long roleId) {
        return toRoleResource(dao.findOne(roleId));
    }

    @Override
    @Transactional
    public RoleResource update(final Long roleId, final UpdateRoleDto updateRole) {
        final RoleEntity entity = dao.findOne(roleId);
        final RoleEntity updated = update(entity, updateRole);
        return toRoleResource(updated);
    }

    @Override
    @Transactional
    public void delete(final Long roleId) {
        dao.delete(roleId);
    }

    private RoleEntity toRoleEntity(final NewRoleDto role) {
        return conversionService.convert(role, RoleEntity.class);
    }

    private RoleEntity update(final RoleEntity entity, final UpdateRoleDto updateRole) {
        entity.setName(updateRole.getName());
        return entity;
    }

    private RoleResource toRoleResource(final RoleEntity entity) {
        return conversionService.convert(entity, RoleResource.class);
    }
}
