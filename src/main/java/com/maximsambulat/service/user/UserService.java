package com.maximsambulat.service.user;

import com.maximsambulat.domain.dto.NewUserDto;
import com.maximsambulat.domain.dto.UpdateUserDto;
import com.maximsambulat.domain.resource.RoleResource;
import com.maximsambulat.domain.resource.UserResource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Maxim Sambulat.
 */
@Service
public interface UserService {
    UserResource create(NewUserDto newUser);

    List<UserResource> getAll();

    UserResource getById(Long clientId);

    UserResource update(Long clientId, UpdateUserDto updateClient);

    void delete(Long clientId);

    void addRole(Long clientId, Long roleId);

    List<RoleResource> getAllRoles(Long clientId);

    void deleteRole(Long clientId, Long roleId);
}
