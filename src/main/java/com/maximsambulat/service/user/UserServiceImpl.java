package com.maximsambulat.service.user;

import com.maximsambulat.dao.RoleDao;
import com.maximsambulat.dao.UserDao;
import com.maximsambulat.domain.dto.NewUserDto;
import com.maximsambulat.domain.dto.UpdateUserDto;
import com.maximsambulat.domain.entity.RoleEntity;
import com.maximsambulat.domain.entity.UserEntity;
import com.maximsambulat.domain.resource.RoleResource;
import com.maximsambulat.domain.resource.UserResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

/**
 * Created by Maxim Sambulat.
 */
@Service
public class UserServiceImpl implements UserService {
    private final UserDao userDao;
    private final RoleDao roleDao;
    private final ConversionService conversionService;

    @Autowired
    public UserServiceImpl(final UserDao userDao,
                           final RoleDao roleDao,
                           final ConversionService conversionService) {
        this.userDao = userDao;
        this.roleDao = roleDao;
        this.conversionService = conversionService;
    }

    @Override
    public UserResource create(NewUserDto newUser) {
        final UserEntity entity = toUserEntity(newUser);
        userDao.save(entity);
        return toUserResource(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserResource> getAll() {
        return userDao.findAll().stream()
            .map(this::toUserResource)
            .collect(toList());
    }

    @Override
    @Transactional(readOnly = true)
    public UserResource getById(final Long clientId) {
        return toUserResource(userDao.findOne(clientId));
    }

    @Override
    @Transactional
    public UserResource update(final Long clientId, final UpdateUserDto updateClient) {
        final UserEntity entity = userDao.findOne(clientId);
        final UserEntity updated = update(entity, updateClient);
        return toUserResource(updated);
    }

    @Override
    @Transactional
    public void delete(final Long clientId) {
        userDao.delete(clientId);
    }

    @Override
    @Transactional
    public void addRole(final Long clientId, final Long roleId) {
        final UserEntity userEntity = userDao.findOne(clientId);
        final RoleEntity roleEntity = roleDao.findOne(roleId);
        final Set<RoleEntity> roles = userEntity.getRoles();
        if (!roles.contains(roleEntity)) {
            roles.add(roleEntity);
        }
    }

    @Override
    @Transactional
    public List<RoleResource> getAllRoles(final Long clientId) {
        final UserEntity userEntity = userDao.findOne(clientId);
        return userEntity.getRoles().stream()
            .map(this::toRoleResource)
            .collect(toList());
    }

    @Override
    @Transactional
    public void deleteRole(final Long clientId, final Long roleId) {
        final UserEntity userEntity = userDao.findOne(clientId);
        userEntity.getRoles().removeIf(role -> role.getId().equals(roleId));
    }

    private UserResource toUserResource(final UserEntity entity) {
        return conversionService.convert(entity, UserResource.class);
    }

    private UserEntity toUserEntity(final NewUserDto newUser) {
        return conversionService.convert(newUser, UserEntity.class);
    }

    private RoleResource toRoleResource(final RoleEntity entity) {
        return conversionService.convert(entity, RoleResource.class);
    }

    private UserEntity update(final UserEntity entity, final UpdateUserDto updateClient) {
        entity.setActive(updateClient.getActive());
        entity.setUsername(updateClient.getUsername());
        entity.setPassword(updateClient.getPassword());
        return entity;
    }
}
