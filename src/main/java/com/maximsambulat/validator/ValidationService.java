package com.maximsambulat.validator;

import org.springframework.stereotype.Service;

@Service
public interface ValidationService {
    <T> void validate(final T obj);
}
