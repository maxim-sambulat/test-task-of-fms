package com.maximsambulat.validator;

import com.maximsambulat.exception.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Service
public class ValidationServiceImpl implements ValidationService {
    private final Validator validator;

    @Autowired
    public ValidationServiceImpl(final Validator validator) {
        this.validator = validator;
    }

    @Override
    public <T> void validate(final T obj) {
        toViolations(validator.validate(obj))
            .ifPresent(violations -> {
                throw new ValidationException(violations, getMessage(obj));
            });
    }

    private <T> Optional<List<Violation>> toViolations(final Set<ConstraintViolation<T>> constraintViolations) {
        return constraintViolations.isEmpty()
               ? Optional.empty()
               : Optional.of(
                   constraintViolations.stream()
                       .map(Violation::new)
                       .collect(toList())
               );
    }

    private <T> String getMessage(final T obj) {
        return "Error of validation of '" + obj.getClass().getCanonicalName() + "'.";
    }
}