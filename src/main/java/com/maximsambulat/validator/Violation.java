package com.maximsambulat.validator;

import lombok.Getter;

import javax.validation.ConstraintViolation;

@Getter
public class Violation {
    private final String propertyPath;
    private final Object invalidValue;
    private final String message;

    public Violation(final ConstraintViolation constraintViolation) {
        propertyPath = constraintViolation.getPropertyPath().toString();
        invalidValue = constraintViolation.getInvalidValue();
        message = constraintViolation.getMessage();
    }
}
