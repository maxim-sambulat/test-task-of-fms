package com.maximsambulat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maximsambulat.configuration.ApplicationTestConfiguration;
import com.maximsambulat.domain.dto.AppendRoleDto;
import com.maximsambulat.domain.dto.NewRoleDto;
import com.maximsambulat.domain.dto.NewUserDto;
import com.maximsambulat.domain.dto.UpdateRoleDto;
import com.maximsambulat.domain.dto.UpdateUserDto;
import com.maximsambulat.domain.resource.RoleResource;
import com.maximsambulat.domain.resource.UserResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Set;

import static com.maximsambulat.controller.RoleController.ROLE_CONTROLLER_URL;
import static com.maximsambulat.controller.UserController.USER_CONTROLLER_URL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource({
    "classpath:/properties/datasource-test.properties",
    "classpath:/properties/application-test.properties"
})
@ContextConfiguration(classes = ApplicationTestConfiguration.class)
@WebAppConfiguration
public class FmsIT {
    private final ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

//    private RoleResource roleAdminResource;
//    private RoleResource roleManagerResource;
//    private UserResource userJohnResource;
//    private UserResource userSmithResource;
//    private UserResource userMarkResource;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
            .build();
    }

    @Test
    public void test() throws Exception {
        final RoleResource roleAdminResource = create(new NewRoleDto("ADMIN"));
        final RoleResource roleSalesManagerResource = create(new NewRoleDto("SALES MANAGER"));
        final RoleResource roleManagerResource = updateRole(roleSalesManagerResource, new UpdateRoleDto("MANAGER"));
        checkCountRoles(roleAdminResource, roleManagerResource);

        final UserResource userJohnResource = create(new NewUserDto(true, "John", "JohnPass"));
        final UserResource userSmithResource = create(new NewUserDto(true, "Smith", "SmithPass"));
        final UserResource userMarkusResource = create(new NewUserDto(true, "Markus", "MarkusPass"));
        final UserResource userMarkResource = updateUser(userMarkusResource, new UpdateUserDto(true, "Mark", "MarkPass"));
        checkCountUsers(userJohnResource, userSmithResource, userMarkResource);

        addRoleToUser(userJohnResource);
        addRoleToUser(userSmithResource, roleAdminResource);
        addRoleToUser(userMarkResource, roleAdminResource, roleManagerResource);

        deleteRoleFromUser(userSmithResource, roleAdminResource, 0);
        deleteRoleFromUser(userMarkResource, roleAdminResource, 1);

        deleteUser(userJohnResource, 2);
        deleteUser(userSmithResource, 1);

        deleteRole(roleAdminResource, 1);
    }

    private RoleResource create(final NewRoleDto dto) throws Exception {
        final MvcResult postResponse = requestPost(ROLE_CONTROLLER_URL, dto);
        final String urlNewRole = postResponse.getResponse().getHeader(HttpHeaders.LOCATION);

        final MvcResult mvcResult = requestGet(urlNewRole);
        final String json = mvcResult.getResponse().getContentAsString();
        final RoleResource result = toObject(json, RoleResource.class);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(dto.getName(), result.getName());

        return result;
    }

    private RoleResource updateRole(final RoleResource role, final UpdateRoleDto dto) throws Exception {
        final MvcResult mvcResult = requestPut(ROLE_CONTROLLER_URL + "/" + role.getId(), dto);
        final String json = mvcResult.getResponse().getContentAsString();
        final RoleResource result = toObject(json, RoleResource.class);

        assertNotNull(result);
        assertEquals(role.getId(), result.getId());
        assertEquals(dto.getName(), result.getName());
        return result;
    }

    private void checkCountRoles(final RoleResource... roles) throws Exception {
        final Set<RoleResource> allRoles =
            findAll(ROLE_CONTROLLER_URL, new TypeReference<Set<RoleResource>>() {
            });
        assertEquals(allRoles.size(), roles.length);
        for (RoleResource role : roles) {
            assertTrue(allRoles.contains(role));
        }
    }

    private UserResource create(final NewUserDto dto) throws Exception {
        final MvcResult postResponse = requestPost(USER_CONTROLLER_URL, dto);
        final String urlNewRole = postResponse.getResponse().getHeader(HttpHeaders.LOCATION);
        final MvcResult mvcResult = requestGet(urlNewRole);
        final String json = mvcResult.getResponse().getContentAsString();
        final UserResource result = toObject(json, UserResource.class);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(dto.getActive(), result.isActive());
        assertEquals(dto.getUsername(), result.getUsername());
        assertEquals(dto.getPassword(), result.getPassword());

        return result;
    }

    private UserResource updateUser(final UserResource user, final UpdateUserDto dto) throws Exception {
        final MvcResult mvcResult = requestPut(USER_CONTROLLER_URL + "/" + user.getId(), dto);
        final String json = mvcResult.getResponse().getContentAsString();
        final UserResource result = toObject(json, UserResource.class);

        assertNotNull(result);
        assertEquals(user.getId(), result.getId());
        assertEquals(dto.getActive(), result.isActive());
        assertEquals(dto.getUsername(), result.getUsername());
        assertEquals(dto.getPassword(), result.getPassword());
        return result;
    }

    private void checkCountUsers(final UserResource... users) throws Exception {
        final Set<UserResource> allUsers =
            findAll(USER_CONTROLLER_URL, new TypeReference<Set<UserResource>>() {
            });
        assertEquals(allUsers.size(), users.length);
        for (UserResource user : users) {
            assertTrue(allUsers.contains(user));
        }
    }

    private void addRoleToUser(final UserResource user, final RoleResource... roles) throws Exception {
        for (RoleResource role : roles) {
            requestPut(
                USER_CONTROLLER_URL + "/" + user.getId() + "/roles",
                new AppendRoleDto(role.getId())
            );
        }

        final Set<RoleResource> rolesOfUser = findAll(
            USER_CONTROLLER_URL + "/" + user.getId() + "/roles",
            new TypeReference<Set<RoleResource>>() {
            }
        );

        assertEquals(rolesOfUser.size(), roles.length);
        for (RoleResource role : roles) {
            assertTrue(rolesOfUser.contains(role));
        }
    }

    private void deleteRoleFromUser(final UserResource user,
                                    final RoleResource role,
                                    final long count) throws Exception {
        requestDelete(USER_CONTROLLER_URL + "/" + user.getId() + "/roles/" + role.getId());

        final Set<RoleResource> roles = findAll(
            USER_CONTROLLER_URL + "/" + user.getId() + "/roles",
            new TypeReference<Set<RoleResource>>() {
            }
        );

        assertEquals(roles.size(), count);
    }

    private void deleteUser(final UserResource user, final long count) throws Exception {
        requestDelete(USER_CONTROLLER_URL + "/" + user.getId());

        final Set<UserResource> users = findAll(
            USER_CONTROLLER_URL,
            new TypeReference<Set<UserResource>>() {
            }
        );

        assertEquals(users.size(), count);
    }

    private void deleteRole(final RoleResource role, final long count) throws Exception {
        requestDelete(ROLE_CONTROLLER_URL + "/" + role.getId());

        final Set<RoleResource> roles = findAll(
            ROLE_CONTROLLER_URL,
            new TypeReference<Set<RoleResource>>() {
            }
        );

        assertEquals(roles.size(), count);
    }

    private <T> Set<T> findAll(final String url, TypeReference reference) throws Exception {
        final MvcResult mvcResult = requestGet(url);
        final String json = mvcResult.getResponse().getContentAsString();
        return toObject(json, reference);
    }

    private <T> MvcResult requestPost(final String url, final T dto) throws Exception {
        return mockMvc.perform(
            post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(dto))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated())
            .andReturn();
    }

    private <T> MvcResult requestPut(final String url, final T dto) throws Exception {
        return mockMvc.perform(
            put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(dto))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    }

    private <T> MvcResult requestGet(final String url) throws Exception {
        return mockMvc.perform(
            get(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    }

    private void requestDelete(final String url) throws Exception {
        mockMvc.perform(delete(url))
            .andExpect(status().isOk());
    }

    private <T> String toJson(final T obj) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }

    private <T> T toObject(final String json, final Class<T> target) throws IOException {
        return mapper.readValue(json, target);
    }

    private <T> T toObject(final String json, final TypeReference reference) throws IOException {
        return mapper.readValue(json, reference);
    }
}
