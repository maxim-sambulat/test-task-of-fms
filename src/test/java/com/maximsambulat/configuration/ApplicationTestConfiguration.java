package com.maximsambulat.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@Import({
    DatabaseConfiguration.class,
    WebMvcConfiguration.class,
    WebSecurityConfiguration.class,
    ServiceConfiguration.class,
    ValidationConfiguration.class,
    ConverterConfiguration.class,
    ErrorHandlerConfiguration.class
})
public class ApplicationTestConfiguration {
}
