package com.maximsambulat.configuration;

import com.maximsambulat.controller.AuthenticationController;
import com.maximsambulat.security.SecurityTokenService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Collections;

import static org.mockito.Mockito.mock;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@Import({
    ValidationConfiguration.class,
    ErrorHandlerConfiguration.class
})
@EnableWebMvc
public class AuthenticationControllerTestConfiguration {
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    @Bean
    public AuthenticationController authenticationController() {
        return new AuthenticationController(userDetailsService(), securityTokenService());
    }

    @Bean
    public SecurityTokenService securityTokenService() {
        return mock(SecurityTokenService.class);
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new InMemoryUserDetailsManager(
            Collections.singletonList(
                new User(
                    USERNAME,
                    PASSWORD,
                    Collections.singletonList(new SimpleGrantedAuthority("ADMIN"))
                )
            )
        );
    }
}
