package com.maximsambulat.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maximsambulat.controller.RoleController;
import com.maximsambulat.service.role.RoleService;
import com.maximsambulat.validator.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.mockito.Mockito.mock;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@Import({
    ValidationConfiguration.class,
    ErrorHandlerConfiguration.class
})
@EnableWebMvc
public class RoleControllerTestConfiguration {
    @Autowired
    private ValidationConfiguration validationConfiguration;

    @Bean
    public RoleService userService() {
        return mock(RoleService.class);
    }

    @Bean
    public RoleController roleController() {
        final RoleService roleService = userService();
        final ValidationService validationService = validationConfiguration.validationService();
        return new RoleController(validationService, roleService);
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
