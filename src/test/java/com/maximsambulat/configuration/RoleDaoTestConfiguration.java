package com.maximsambulat.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@Import({
    DatabaseConfiguration.class
})
public class RoleDaoTestConfiguration {
}
