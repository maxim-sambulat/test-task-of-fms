package com.maximsambulat.configuration;

import com.maximsambulat.dao.RoleDao;
import com.maximsambulat.service.role.RoleService;
import com.maximsambulat.service.role.RoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import static org.mockito.Mockito.mock;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@Import({
    ConverterConfiguration.class
})
public class RoleServiceTestConfiguration {
    private final ConverterConfiguration converterConfiguration;

    @Autowired
    public RoleServiceTestConfiguration(final ConverterConfiguration converterConfiguration) {
        this.converterConfiguration = converterConfiguration;
    }

    @Bean
    public RoleDao roleDao() {
        return mock(RoleDao.class);
    }

    @Bean
    public RoleService roleService() {
        return new RoleServiceImpl(
            roleDao(),
            converterConfiguration.conversionService().getObject()
        );
    }
}
