package com.maximsambulat.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maximsambulat.controller.UserController;
import com.maximsambulat.service.user.UserService;
import com.maximsambulat.validator.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.mockito.Mockito.mock;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@Import({
    ValidationConfiguration.class,
    ErrorHandlerConfiguration.class
})
@EnableWebMvc
public class UserControllerTestConfiguration {
    @Autowired
    private ValidationConfiguration validationConfiguration;

    @Bean
    public UserService userService() {
        return mock(UserService.class);
    }

    @Bean
    public UserController userController() {
        final UserService userService = userService();
        final ValidationService validationService = validationConfiguration.validationService();
        return new UserController(validationService, userService);
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
