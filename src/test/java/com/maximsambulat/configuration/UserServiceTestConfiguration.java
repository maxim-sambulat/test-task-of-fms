package com.maximsambulat.configuration;

import com.maximsambulat.dao.RoleDao;
import com.maximsambulat.dao.UserDao;
import com.maximsambulat.service.user.UserService;
import com.maximsambulat.service.user.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import static org.mockito.Mockito.mock;

/**
 * Created by Maxim Sambulat.
 */
@Configuration
@Import({
    ConverterConfiguration.class
})
public class UserServiceTestConfiguration {
    private final ConverterConfiguration converterConfiguration;

    @Autowired
    public UserServiceTestConfiguration(final ConverterConfiguration converterConfiguration) {
        this.converterConfiguration = converterConfiguration;
    }

    @Bean
    public RoleDao roleDao() {
        return mock(RoleDao.class);
    }

    @Bean
    public UserDao userDao() {
        return mock(UserDao.class);
    }

    @Bean
    public UserService roleService() {
        return new UserServiceImpl(
            userDao(),
            roleDao(),
            converterConfiguration.conversionService().getObject()
        );
    }
}
