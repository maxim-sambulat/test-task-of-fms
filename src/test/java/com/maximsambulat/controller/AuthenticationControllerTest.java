package com.maximsambulat.controller;

import com.maximsambulat.configuration.AuthenticationControllerTestConfiguration;
import com.maximsambulat.security.SecurityTokenService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.maximsambulat.configuration.AuthenticationControllerTestConfiguration.PASSWORD;
import static com.maximsambulat.configuration.AuthenticationControllerTestConfiguration.USERNAME;
import static com.maximsambulat.constant.WebConstant.AUTHENTICATE_HEADER_NAME;
import static com.maximsambulat.constant.WebConstant.QUERY_PARAMETER_PASSWORD;
import static com.maximsambulat.constant.WebConstant.QUERY_PARAMETER_USERNAME;
import static com.maximsambulat.controller.AuthenticationController.AUTHENTICATION_CONTROLLER_URL;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(SpringRunner.class)
@TestPropertySource({
    "classpath:/properties/application-test.properties"
})
@ContextConfiguration(classes = AuthenticationControllerTestConfiguration.class)
@WebAppConfiguration
public class AuthenticationControllerTest {
    private static final String JWT_TOKEN = "JWT_TOKEN#1234";

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private SecurityTokenService securityTokenService;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
            .build();
    }

    @Test
    public void loginOk() throws Exception {
        when(securityTokenService.getJwtToken(anyString()))
            .thenReturn(JWT_TOKEN);

        mockMvc.perform(
            post(AUTHENTICATION_CONTROLLER_URL)
                .param(QUERY_PARAMETER_USERNAME, USERNAME)
                .param(QUERY_PARAMETER_PASSWORD, PASSWORD))
            .andExpect(status().isOk())
            .andExpect(
                header().string(
                    AUTHENTICATE_HEADER_NAME,
                    JWT_TOKEN
                )
            );
    }

    @Test
    public void loginUsernameNotFound() throws Exception {
        when(securityTokenService.getJwtToken(anyString()))
            .thenReturn(JWT_TOKEN);

        mockMvc.perform(
            post(AUTHENTICATION_CONTROLLER_URL)
                .param(QUERY_PARAMETER_USERNAME, "")
                .param(QUERY_PARAMETER_PASSWORD, PASSWORD))
            .andExpect(status().isNotFound())
        ;
    }

    @Test
    public void loginUnauthorized() throws Exception {
        when(securityTokenService.getJwtToken(anyString()))
            .thenReturn(JWT_TOKEN);

        mockMvc.perform(
            post(AUTHENTICATION_CONTROLLER_URL)
                .param(QUERY_PARAMETER_USERNAME, USERNAME)
                .param(QUERY_PARAMETER_PASSWORD, ""))
            .andExpect(status().isUnauthorized())
        ;
    }
}
