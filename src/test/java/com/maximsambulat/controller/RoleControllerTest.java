package com.maximsambulat.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maximsambulat.configuration.RoleControllerTestConfiguration;
import com.maximsambulat.domain.dto.NewRoleDto;
import com.maximsambulat.domain.dto.UpdateRoleDto;
import com.maximsambulat.domain.resource.RoleResource;
import com.maximsambulat.service.role.RoleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.maximsambulat.controller.RoleController.ROLE_CONTROLLER_URL;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = RoleControllerTestConfiguration.class)
@WebAppConfiguration
public class RoleControllerTest {
    private static final Long ROLE_ID_1 = 10L;
    private static final String ROLE_NAME_1 = "ADMIN";
    private static final Long ROLE_ID_2 = 20L;
    private static final String ROLE_NAME_2 = "MANAGER";
    private static final Long UNKNOWN_ROLE_ID = 30L;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private RoleService roleService;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
            .build();
    }

    @Test
    public void testCreate() throws Exception {
        final RoleResource dto = getRoleResource();
        when(roleService.create(any(NewRoleDto.class)))
            .thenReturn(dto);
        final NewRoleDto newRoleDto = getNewRoleDto();
        mockMvc.perform(
            post(ROLE_CONTROLLER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(newRoleDto))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated())
            .andExpect(
                header().string(
                    HttpHeaders.LOCATION,
                    ServletUriComponentsBuilder
                        .fromCurrentRequest().path(ROLE_CONTROLLER_URL + "/{id}")
                        .buildAndExpand(dto.getId()).toUriString()
                )
            );
        verify(roleService).create(newRoleDto);
    }

    private NewRoleDto getNewRoleDto() {
        return new NewRoleDto(ROLE_NAME_1);
    }

    @Test
    public void testCreateValidationException() throws Exception {
        String json = toJson(new NewRoleDto());
        mockMvc.perform(
            post(ROLE_CONTROLLER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateEntityExistsException() throws Exception {
        final NewRoleDto duplicateNewRoleDto = getDuplicateNewRoleDto();
        doThrow(EntityExistsException.class)
            .when(roleService)
            .create(duplicateNewRoleDto);
        mockMvc.perform(
            post(ROLE_CONTROLLER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(duplicateNewRoleDto))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isConflict());
    }

    private NewRoleDto getDuplicateNewRoleDto() {
        return new NewRoleDto("");
    }

    @Test
    public void testGetAll() throws Exception {
        when(roleService.getAll())
            .thenReturn(getRoleResources());
        mockMvc.perform(
            get(ROLE_CONTROLLER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.size()").value(2))
            .andExpect(jsonPath("$.[0].id", equalTo(ROLE_ID_1.intValue())))
            .andExpect(jsonPath("$.[0].name").value(ROLE_NAME_1))
            .andExpect(jsonPath("$.[1].id").value(ROLE_ID_2.intValue()))
            .andExpect(jsonPath("$.[1].name").value(ROLE_NAME_2));
        verify(roleService).getAll();
    }

    private List<RoleResource> getRoleResources() {
        final List<RoleResource> result = new ArrayList<>();
        result.add(new RoleResource(ROLE_ID_1, ROLE_NAME_1));
        result.add(new RoleResource(ROLE_ID_2, ROLE_NAME_2));
        return result;
    }

    @Test
    public void testGetById() throws Exception {
        when(roleService.getById(anyLong()))
            .thenReturn(getRoleResource());
        mockMvc.perform(
            get(ROLE_CONTROLLER_URL + "/{roleId}", ROLE_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(ROLE_ID_1.intValue()))
            .andExpect(jsonPath("$.name").value(ROLE_NAME_1));
        verify(roleService).getById(ROLE_ID_1);
    }

    @Test
    public void testGetByIdEntityNotFound() throws Exception {
        doThrow(EntityNotFoundException.class)
            .when(roleService)
            .getById(UNKNOWN_ROLE_ID);
        mockMvc.perform(
            get(ROLE_CONTROLLER_URL + "/{roleId}", UNKNOWN_ROLE_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNotFound());
        verify(roleService).getById(UNKNOWN_ROLE_ID);
    }

    @Test
    public void testUpdate() throws Exception {
        final UpdateRoleDto updateRoleDto = getUpdateRoleDto();
        when(roleService.update(anyLong(), any(UpdateRoleDto.class)))
            .thenReturn(getRoleResource());
        mockMvc.perform(
            put(ROLE_CONTROLLER_URL + "/{clientId}", ROLE_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(updateRoleDto))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(ROLE_ID_1.intValue()))
            .andExpect(jsonPath("$.name").value(ROLE_NAME_1));
        verify(roleService).update(ROLE_ID_1, updateRoleDto);
    }

    @Test
    public void testUpdateValidationException() throws Exception {
        mockMvc.perform(
            put(ROLE_CONTROLLER_URL + "/{clientId}", ROLE_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new UpdateRoleDto()))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateEntityNotFound() throws Exception {
        final UpdateRoleDto updateRoleDto = getUpdateRoleDto();
        doThrow(EntityNotFoundException.class)
            .when(roleService)
            .update(eq(UNKNOWN_ROLE_ID), any(UpdateRoleDto.class));
        mockMvc.perform(
            put(ROLE_CONTROLLER_URL + "/{clientId}", UNKNOWN_ROLE_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(updateRoleDto))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNotFound());
        verify(roleService).update(eq(UNKNOWN_ROLE_ID), any(UpdateRoleDto.class));
    }

    private UpdateRoleDto getUpdateRoleDto() {
        return new UpdateRoleDto(ROLE_NAME_1);
    }

    @Test
    public void testDelete() throws Exception {
        mockMvc.perform(
            delete(ROLE_CONTROLLER_URL +"/{roleId}", ROLE_ID_1))
            .andExpect(status().isOk());
        verify(roleService).delete(ROLE_ID_1);
    }

    @Test
    public void testDeleteEntityNotFound() throws Exception {
        doThrow(EntityNotFoundException.class)
            .when(roleService)
            .delete(UNKNOWN_ROLE_ID);
        mockMvc.perform(
            delete(ROLE_CONTROLLER_URL + "/{roleId}", UNKNOWN_ROLE_ID))
            .andExpect(status().isNotFound());
        verify(roleService).delete(UNKNOWN_ROLE_ID);
    }

    private RoleResource getRoleResource() {
        return new RoleResource(ROLE_ID_1, ROLE_NAME_1);
    }

    private <T> String toJson(final T obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}
