package com.maximsambulat.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maximsambulat.configuration.UserControllerTestConfiguration;
import com.maximsambulat.domain.dto.NewUserDto;
import com.maximsambulat.domain.dto.UpdateUserDto;
import com.maximsambulat.domain.resource.RoleResource;
import com.maximsambulat.domain.resource.UserResource;
import com.maximsambulat.service.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.maximsambulat.controller.UserController.USER_CONTROLLER_URL;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UserControllerTestConfiguration.class)
@WebAppConfiguration
public class UserControllerTest {
    private static final Long CLIENT_ID_1 = 1L;
    private static final Long CLIENT_ID_2 = 2L;
    private static final Long UNKNOWN_CLIENT_ID = 3L;
    private static final String USERNAME_1 = "User 1";
    private static final String PASSWORD_1 = "Password 1";
    private static final String USERNAME_2 = "User 2";
    private static final String PASSWORD_2 = "Password 2";
    private static final Long ROLE_ID_1 = 10L;
    private static final String ROLE_NAME_1 = "ADMIN";
    private static final Long ROLE_ID_2 = 20L;
    private static final String ROLE_NAME_2 = "MANAGER";
    private static final Long UNKNOWN_ROLE_ID = 30L;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
            .build();
    }

    @Test
    public void create() throws Exception {
        final UserResource dto = getUserResource();
        when(userService.create(any(NewUserDto.class)))
            .thenReturn(dto);

        final NewUserDto newUserDto = getNewUserDto();
        mockMvc.perform(
            post(USER_CONTROLLER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(newUserDto))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated())
            .andExpect(
                header().string(
                    HttpHeaders.LOCATION,
                    ServletUriComponentsBuilder
                        .fromCurrentRequest().path(USER_CONTROLLER_URL + "/{id}")
                        .buildAndExpand(dto.getId()).toUriString()
                )
            );
        verify(userService).create(newUserDto);
    }

    private NewUserDto getNewUserDto() {
        return new NewUserDto(true, USERNAME_1, PASSWORD_2);
    }

    @Test
    public void createValidationException() throws Exception {
        mockMvc.perform(
            post(USER_CONTROLLER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new NewUserDto()))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void createEntityExistsException() throws Exception {
        final NewUserDto duplicateNewUserDto = getDuplicateNewUserDto();
        doThrow(EntityExistsException.class)
            .when(userService)
            .create(duplicateNewUserDto);
        mockMvc.perform(
            post(USER_CONTROLLER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(duplicateNewUserDto))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isConflict());
    }

    private NewUserDto getDuplicateNewUserDto() {
        return new NewUserDto(true, "", "");
    }

    @Test
    public void getAll() throws Exception {
        when(userService.getAll())
            .thenReturn(getUserResources());
        mockMvc.perform(
            get(USER_CONTROLLER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.size()").value(2))
            .andExpect(jsonPath("$.[0].id").value(CLIENT_ID_1.intValue()))
            .andExpect(jsonPath("$.[0].active").value(true))
            .andExpect(jsonPath("$.[0].username").value(USERNAME_1))
            .andExpect(jsonPath("$.[0].password").value(PASSWORD_1))
            .andExpect(jsonPath("$.[1].id").value(CLIENT_ID_2.intValue()))
            .andExpect(jsonPath("$.[1].active").value(true))
            .andExpect(jsonPath("$.[1].username").value(USERNAME_2))
            .andExpect(jsonPath("$.[1].password").value(PASSWORD_2));
        verify(userService).getAll();
    }

    private List<UserResource> getUserResources() {
        final List<UserResource> users = new ArrayList<>();
        users.add(new UserResource(CLIENT_ID_1, true, USERNAME_1, PASSWORD_1));
        users.add(new UserResource(CLIENT_ID_2, true, USERNAME_2, PASSWORD_2));
        return users;
    }

    @Test
    public void getById() throws Exception {
        when(userService.getById(anyLong()))
            .thenReturn(getUserResource());
        mockMvc.perform(
            get(USER_CONTROLLER_URL + "/{clientId}", CLIENT_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(CLIENT_ID_1.intValue()))
            .andExpect(jsonPath("$.active").value(true))
            .andExpect(jsonPath("$.username").value(USERNAME_1))
            .andExpect(jsonPath("$.password").value(PASSWORD_2));
        verify(userService).getById(CLIENT_ID_1);
    }

    @Test
    public void getByIdEntityNotFound() throws Exception {
        doThrow(EntityNotFoundException.class)
            .when(userService)
            .getById(UNKNOWN_CLIENT_ID);
        mockMvc.perform(
            get(USER_CONTROLLER_URL + "/{clientId}", UNKNOWN_CLIENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNotFound());
        verify(userService).getById(UNKNOWN_CLIENT_ID);
    }

    @Test
    public void update() throws Exception {
        final UpdateUserDto updateClientDto = getUpdateUserDto();
        when(userService.update(CLIENT_ID_1, updateClientDto))
            .thenReturn(getUserResource());
        mockMvc.perform(
            put(USER_CONTROLLER_URL + "/{clientId}", CLIENT_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(updateClientDto))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(CLIENT_ID_1.intValue()))
            .andExpect(jsonPath("$.active").value(true))
            .andExpect(jsonPath("$.username").value(USERNAME_1))
            .andExpect(jsonPath("$.password").value(PASSWORD_2));
        verify(userService).update(CLIENT_ID_1, updateClientDto);
    }

    @Test
    public void updateValidationException() throws Exception {
        mockMvc.perform(
            put(USER_CONTROLLER_URL + "/{clientId}", CLIENT_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new UpdateUserDto()))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isBadRequest());
    }

    @Test
    public void updateEntityNotFound() throws Exception {
        final UpdateUserDto updateClientDto = getUpdateUserDto();
        doThrow(EntityNotFoundException.class)
            .when(userService)
            .update(UNKNOWN_CLIENT_ID, updateClientDto);
        mockMvc.perform(
            put(USER_CONTROLLER_URL + "/{clientId}", UNKNOWN_CLIENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(updateClientDto))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNotFound());
        verify(userService).update(UNKNOWN_CLIENT_ID, updateClientDto);
    }

    private UpdateUserDto getUpdateUserDto() {
        return new UpdateUserDto(true, USERNAME_1, PASSWORD_2);
    }

    @Test
    public void delete() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.delete(USER_CONTROLLER_URL + "/{clientId}", CLIENT_ID_1))
            .andExpect(status().isOk());
        verify(userService).delete(CLIENT_ID_1);
    }

    @Test
    public void deleteEntityNotFound() throws Exception {
        doThrow(EntityNotFoundException.class)
            .when(userService)
            .delete(UNKNOWN_CLIENT_ID);
        mockMvc.perform(
            MockMvcRequestBuilders.delete(USER_CONTROLLER_URL + "/{clientId}", UNKNOWN_CLIENT_ID))
            .andExpect(status().isNotFound());
        verify(userService).delete(UNKNOWN_CLIENT_ID);
    }

    @Test
    public void addRole() throws Exception {
        mockMvc.perform(
            put(USER_CONTROLLER_URL + "/{clientId}/roles", CLIENT_ID_1).param("id", ROLE_ID_1.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new RoleResource(ROLE_ID_1, ROLE_NAME_1)))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
        verify(userService).addRole(CLIENT_ID_1, ROLE_ID_1);
    }

    @Test
    public void addRoleEntityNotFound() throws Exception {
        doThrow(EntityNotFoundException.class)
            .when(userService)
            .addRole(CLIENT_ID_1, UNKNOWN_ROLE_ID);
        mockMvc.perform(
            put(USER_CONTROLLER_URL + "/{clientId}/roles", CLIENT_ID_1).param("id", UNKNOWN_ROLE_ID.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new RoleResource(UNKNOWN_ROLE_ID, ROLE_NAME_1)))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
        verify(userService).addRole(CLIENT_ID_1, UNKNOWN_ROLE_ID);
    }

    @Test
    public void getAllRoles() throws Exception {
        when(userService.getAllRoles(anyLong()))
            .thenReturn(getRoleResources());
        mockMvc.perform(
            get(USER_CONTROLLER_URL + "/{clientId}/roles", CLIENT_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.size()").value(2))
            .andExpect(jsonPath("$.[0].id", equalTo(ROLE_ID_1.intValue())))
            .andExpect(jsonPath("$.[0].name").value(ROLE_NAME_1))
            .andExpect(jsonPath("$.[1].id").value(ROLE_ID_2.intValue()))
            .andExpect(jsonPath("$.[1].name").value(ROLE_NAME_2));
        verify(userService).getAllRoles(CLIENT_ID_1);
    }

    @Test
    public void deleteRole() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.delete(USER_CONTROLLER_URL + "/{clientId}/roles/{roleId}", CLIENT_ID_1, ROLE_ID_1))
            .andExpect(status().isOk());
        verify(userService).deleteRole(CLIENT_ID_1, ROLE_ID_1);
    }

    @Test
    public void deleteRoleEntityNotFound() throws Exception {
        doThrow(EntityNotFoundException.class)
            .when(userService)
            .deleteRole(CLIENT_ID_1, UNKNOWN_ROLE_ID);
        mockMvc.perform(
            MockMvcRequestBuilders.delete(USER_CONTROLLER_URL + "/{clientId}/roles/{roleId}", CLIENT_ID_1, UNKNOWN_ROLE_ID))
            .andExpect(status().isNotFound());
    }

    private UserResource getUserResource() {
        return new UserResource(1L, true, USERNAME_1, PASSWORD_2);
    }

    private List<RoleResource> getRoleResources() {
        final List<RoleResource> result = new ArrayList<>();
        result.add(new RoleResource(ROLE_ID_1, ROLE_NAME_1));
        result.add(new RoleResource(ROLE_ID_2, ROLE_NAME_2));
        return result;
    }

    private <T> String toJson(final T obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}
