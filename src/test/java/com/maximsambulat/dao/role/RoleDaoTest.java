package com.maximsambulat.dao.role;

import com.maximsambulat.configuration.RoleDaoTestConfiguration;
import com.maximsambulat.dao.RoleDao;
import com.maximsambulat.domain.entity.RoleEntity;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import javax.persistence.PersistenceException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource({
    "classpath:/properties/datasource-test.properties"
})
@ContextConfiguration(classes = RoleDaoTestConfiguration.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RoleDaoTest {
    private static final String ROLE_NAME_1 = "ADMIN";
    private static final String ROLE_NAME_2 = "MANAGER";
    private static final String ROLE_NAME_3 = "SUPPORT";
    private static final String NEW_ROLE_NAME = "SALE";

    @Resource
    private RoleDao roleDao;

    @Test
    @Commit
    public void test1_save() {
        for (RoleEntity entity : dataRoles()) {
            assertNull(entity.getId());
            roleDao.save(entity);
            assertNotNull(entity.getId());
        }
    }

    @Test(expected = PersistenceException.class)
    public void test2_saveEntityExist() {
        final RoleEntity roleAdminDuplicate = getRoleEntity(ROLE_NAME_1);
        roleDao.save(roleAdminDuplicate);
        roleDao.flush();
    }

    @Test
    public void test3_findAll() {
        final List<RoleEntity> entities = roleDao.findAll();
        assertNotNull(entities);
        assertEquals(entities.size(), 3L);
    }

    @Test
    public void test4_findOne() {
        final RoleEntity entity = roleDao.findOne(1L);
        assertNotNull(entity);
        assertEquals(entity.getId().longValue(), 1L);
        assertEquals(entity.getName(), ROLE_NAME_1);
    }

    @Test
    @Commit
    public void test5_update() {
        final RoleEntity entity = roleDao.findOne(1L);
        assertNotNull(entity);
        assertEquals(ROLE_NAME_1, entity.getName());

        entity.setName(NEW_ROLE_NAME);
        roleDao.save(entity);
    }

    @Test
    public void test6_checkUpdate() {
        final RoleEntity updated = roleDao.findOne(1L);
        assertEquals(NEW_ROLE_NAME, updated.getName());
    }

    @Test
    @Commit
    public void test7_delete() {
        final List<RoleEntity> entitiesBeforeDelete = roleDao.findAll();
        assertEquals(entitiesBeforeDelete.size(), 3);
        roleDao.delete(1L);
    }

    @Test
    public void test8_checkDelete() {
        final List<RoleEntity> entitiesAfterDelete = roleDao.findAll();
        assertEquals(entitiesAfterDelete.size(), 2);
    }

    public RoleEntity[] dataRoles() {
        return new RoleEntity[] {
            getRoleEntity(ROLE_NAME_1),
            getRoleEntity(ROLE_NAME_2),
            getRoleEntity(ROLE_NAME_3)
        };
    }

    private RoleEntity getRoleEntity(final String name) {
        final RoleEntity entity = new RoleEntity();
        entity.setName(name);
        return entity;
    }
}
