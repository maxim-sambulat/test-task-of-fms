package com.maximsambulat.dao.user;

import com.maximsambulat.configuration.UserDaoTestConfiguration;
import com.maximsambulat.dao.UserDao;
import com.maximsambulat.domain.entity.RoleEntity;
import com.maximsambulat.domain.entity.UserEntity;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.PersistenceException;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource({
    "classpath:/properties/datasource-test.properties"
})
@ContextConfiguration(classes = UserDaoTestConfiguration.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserDaoTest {
    private static final Long ROLE_ID_1 = 1L;
    private static final Long ROLE_ID_2 = 2L;
    private static final Long ROLE_ID_3 = 3L;
    private static final String ROLE_NAME_1 = "ADMIN";
    private static final String ROLE_NAME_2 = "MANAGER";
    private static final String ROLE_NAME_3 = "SUPPORT";
    private static final String NEW_PASSWORD = "NEW_PASSWORD";

    @Resource
    private UserDao userDao;

    @Test
    @Commit
    @Sql(
        statements = {
            "insert into role(id, name) values(1, 'ADMIN')",
            "insert into role(id, name) values(2, 'MANAGER')",
            "insert into role(id, name) values(3, 'SUPPORT')"
        }
    )
    public void test1_save() {
        for (UserEntity entity : dataUsers()) {
            assertNull(entity.getId());
            userDao.save(entity);
            assertNotNull(entity.getId());
        }
    }

    @Test(expected = PersistenceException.class)
    public void test2_saveEntityExist() {
        final UserEntity userDuplicate = getUserEntity(true, "User 1", "Pass 1");
        userDao.save(userDuplicate);
        userDao.flush();
    }

    @Test
    public void test3_findAll() {
        final List<UserEntity> entities = userDao.findAll();
        assertNotNull(entities);
        assertEquals(entities.size(), 5L);
    }

    @Test
    @Transactional
    public void test4_findOne() {
        final Long userOne = 1L;
        final UserEntity entityOne = userDao.findOne(userOne);
        assertNotNull(entityOne);
        assertEquals(entityOne.getId(), userOne);
        assertEquals(entityOne.getRoles().size(), 2);

        final Long userTwo = 2L;
        final UserEntity entityTwo = userDao.findOne(userTwo);
        assertNotNull(entityTwo);
        assertEquals(entityTwo.getId(), userTwo);
        assertEquals(entityTwo.getRoles().size(), 1);

        final Long userThree = 3L;
        final UserEntity entityThree = userDao.findOne(userThree);
        assertNotNull(entityThree);
        assertEquals(entityThree.getId(), userThree);
        assertEquals(entityThree.getRoles().size(), 0);
    }

    @Test
    @Commit
    public void test5_update() {
        final UserEntity entity = userDao.findOne(1L);
        assertNotNull(entity);
        entity.setPassword(NEW_PASSWORD);
        userDao.save(entity);
    }

    @Test
    public void test6_checkUpdate() {
        final UserEntity updated = userDao.findOne(1L);
        assertEquals(updated.getPassword(), NEW_PASSWORD);
    }

    @Test
    @Commit
    public void test7_delete() {
        final List<UserEntity> entitiesBeforeDelete = userDao.findAll();
        assertEquals(entitiesBeforeDelete.size(), 5);
        userDao.delete(1L);
    }

    @Test
    public void test8_checkDelete() {
        final List<UserEntity> entities = userDao.findAll();
        assertEquals(entities.size(), 4);
    }

    private UserEntity[] dataUsers() {
        return new UserEntity[] {
            getUserEntity(true, "User 1", "Pass 1",
                new RoleEntity(ROLE_ID_1, ROLE_NAME_1),
                new RoleEntity(ROLE_ID_2, ROLE_NAME_2)
            ),
            getUserEntity(false, "User 2", "Pass 2",
                new RoleEntity(ROLE_ID_3, ROLE_NAME_3)
            ),
            getUserEntity(true, "User 3", "Pass 3"),
            getUserEntity(false, "User 4", "Pass 4"),
            getUserEntity(true, "User 5", "Pass 5")
        };
    }

    private UserEntity getUserEntity(final boolean active,
                                     final String username,
                                     final String password,
                                     final RoleEntity... roles) {
        final UserEntity entity = new UserEntity();
        entity.setActive(active);
        entity.setUsername(username);
        entity.setPassword(password);
        if (roles.length > 0) {
            entity.setRoles(
                Stream.of(roles)
                    .collect(toSet())
            );
        }
        return entity;
    }
}
