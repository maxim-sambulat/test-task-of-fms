package com.maximsambulat.security;

import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationServiceException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(JMockit.class)
public class SecurityTokenServiceTest {
    public static final String TEST_USER = "TEST_USER";

    @Test
    public void getJwtToken(@Mocked final Environment environment) {
        final SecurityTokenService service = getSecurityTokenService(environment, 60);
        final String jwtToken = service.getJwtToken(TEST_USER);
        assertNotNull(jwtToken);
    }

    @Test
    public void getToken(@Mocked final Environment environment) {
        final SecurityTokenService service = getSecurityTokenService(environment, 60);
        final String jwtToken = service.getJwtToken(TEST_USER);
        final Token token = service.getToken(jwtToken);
        assertEquals(token.getUsername(), TEST_USER);
    }

    @Test(expected = AuthenticationServiceException.class)
    public void getTokenNonUser(@Mocked final Environment environment) {
        final SecurityTokenService service = getSecurityTokenService(environment, 60);
        final String jwtToken = service.getJwtToken(null);
        service.getToken(jwtToken);
    }

    @Test(expected = AuthenticationServiceException.class)
    public void getTokenInvalidJwtToken(@Mocked final Environment environment) {
        final SecurityTokenService service = new SecurityTokenServiceImpl(environment);
        service.getToken("UNKNOWN");
    }

    private SecurityTokenServiceImpl getSecurityTokenService(final Environment environment, final long seconds) {
        new Expectations() {{
            environment.getProperty("token.expiry_date");
            result = seconds;
        }};
        return new SecurityTokenServiceImpl(environment);
    }
}
