package com.maximsambulat.security;

import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.maximsambulat.constant.WebConstant.AUTHENTICATE_HEADER_NAME;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(JMockit.class)
public class TokenAuthenticationFilterTest {
    private static final String JWT_TOKEN = "JWT_TOKEN";

    @Test(expected = BadCredentialsException.class)
    public void attemptAuthenticationBadCredentials(
        @Mocked final HttpServletRequest request,
        @Mocked final HttpServletResponse response) throws IOException, ServletException {

        new Expectations() {{
            request.getHeader(AUTHENTICATE_HEADER_NAME);
            result = null;
        }};

        final TokenAuthenticationFilter filter = new TokenAuthenticationFilter();
        filter.attemptAuthentication(request, response);
    }

    @Test
    public void attemptAuthentication(
        @Mocked final HttpServletRequest request,
        @Mocked final HttpServletResponse response,
        @Mocked final AuthenticationManager manager) throws IOException, ServletException {

        new Expectations() {{
            request.getHeader(AUTHENTICATE_HEADER_NAME);
            result = JWT_TOKEN;
            manager.authenticate((Authentication) any);
            result = new TokenAuthentication(JWT_TOKEN);

        }};

        final TokenAuthenticationFilter filter = new TokenAuthenticationFilter();
        filter.setAuthenticationManager(manager);
        final Authentication authentication = filter.attemptAuthentication(request, response);
        assertNotNull(authentication);
    }
}
