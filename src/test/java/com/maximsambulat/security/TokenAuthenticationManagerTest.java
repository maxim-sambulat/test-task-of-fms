package com.maximsambulat.security;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(JMockit.class)
public class TokenAuthenticationManagerTest {
    private static final String JWT_TOKEN = "JWT_TOKEN";
    private static final String USERNAME = "John";
    private static final String PASSWORD = "password";

    @Tested
    private TokenAuthenticationManager tokenAuthenticationManager;
    @Injectable
    private UserDetailsService userDetailsService;
    @Injectable
    private SecurityTokenService tokenService;

    @Test
    public void authenticateUnknownAuthentication() {
        final Authentication testingAuthenticationToken = new TestingAuthenticationToken(null, null);
        Authentication authenticate = tokenAuthenticationManager.authenticate(testingAuthenticationToken);
        assertFalse(testingAuthenticationToken.isAuthenticated());
    }

    @Test(expected = DisabledException.class)
    public void authenticateUserIsDisabled() {
        final UserDetails user = new User(
            USERNAME,
            PASSWORD, false, true, true, true,
            Collections.singletonList(new SimpleGrantedAuthority("ADMIN"))
        );
        new Expectations() {{
            tokenService.getToken(JWT_TOKEN);
            result = new Token(USERNAME);
            userDetailsService.loadUserByUsername(USERNAME);
            result = user;
        }};
        final TokenAuthentication authentication = new TokenAuthentication(JWT_TOKEN, false, user);
        tokenAuthenticationManager.authenticate(authentication);
    }

    @Test
    public void authenticate() {
        final UserDetails user = new User(
            USERNAME,
            PASSWORD,
            Collections.singletonList(new SimpleGrantedAuthority("ADMIN"))
        );
        new Expectations() {{
            tokenService.getToken(JWT_TOKEN);
            result = new Token(USERNAME);
            tokenService.getJwtToken(USERNAME);
            result = JWT_TOKEN;
            userDetailsService.loadUserByUsername(USERNAME);
            result = user;
        }};

        final TokenAuthentication authentication = new TokenAuthentication(JWT_TOKEN, false, user);
        final TokenAuthentication tokenAuthentication =
            (TokenAuthentication) tokenAuthenticationManager.authenticate(authentication);
        assertTrue(tokenAuthentication.isAuthenticated());
        assertEquals(JWT_TOKEN, tokenAuthentication.getToken());
        assertEquals(user, tokenAuthentication.getPrincipal());
    }
}
