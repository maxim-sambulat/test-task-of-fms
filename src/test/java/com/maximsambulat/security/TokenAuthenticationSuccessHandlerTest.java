package com.maximsambulat.security;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.Authentication;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(JMockit.class)
public class TokenAuthenticationSuccessHandlerTest {
    private static final String SERVLET_PATH = "http://localhost:8080/app/api/users";
    private static final String PATH_INFO = "/attributes";
    private static final String QUERY_PARAMS = "email=example@gmail.com";
    private static final String URL_FULL = SERVLET_PATH + PATH_INFO + "?" + QUERY_PARAMS;
    private static final String URL_WITHOUT_PATH_INFO = SERVLET_PATH + "?" + QUERY_PARAMS;
    private static final String URL_WITHOUT_QUERY_PARAMS = SERVLET_PATH + PATH_INFO;

    @Test
    public void onAuthenticationSuccess(@Mocked final HttpServletRequest request,
                                        @Mocked final HttpServletResponse response,
                                        @Mocked final Authentication authentication) throws
        ServletException, IOException {

        for (String[] data : getData()) {
            expectations(request, data);
            TokenAuthenticationSuccessHandler handler = new TokenAuthenticationSuccessHandler();
            handler.onAuthenticationSuccess(request, response, authentication);

            new Verifications() {{
                String url;
                request.getRequestDispatcher(url = withCapture());
                assertEquals(data[3], url);
            }};
        }
    }

    private void expectations(final HttpServletRequest request, final String[] data) {
        new Expectations() {{
            request.getServletPath();
            result = data[0];
            request.getPathInfo();
            result = data[1];
            request.getQueryString();
            result = data[2];
        }};
    }

    private String[][] getData() {
        return new String[][] {
            {SERVLET_PATH, PATH_INFO, QUERY_PARAMS, URL_FULL},
            {SERVLET_PATH, null, QUERY_PARAMS, URL_WITHOUT_PATH_INFO},
            {SERVLET_PATH, PATH_INFO, null, URL_WITHOUT_QUERY_PARAMS}
        };
    }
}
