package com.maximsambulat.service.role;

import com.maximsambulat.configuration.RoleServiceTestConfiguration;
import com.maximsambulat.dao.RoleDao;
import com.maximsambulat.domain.dto.NewRoleDto;
import com.maximsambulat.domain.dto.UpdateRoleDto;
import com.maximsambulat.domain.entity.RoleEntity;
import com.maximsambulat.domain.resource.RoleResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = RoleServiceTestConfiguration.class)
public class RoleServiceTest {
    private static final String NAME_ROLE = "SUPPORT";
    private static final Long ROLE_ID_1 = 1L;
    private static final Long ROLE_ID_2 = 2L;
    private static final String ROLE_NAME_1 = "ADMIN";
    private static final String ROLE_NAME_2 = "MANAGER";

    @Autowired
    private RoleService service;
    @Autowired
    private RoleDao roleDao;

    @Test
    public void create() {
        when(roleDao.save(any(RoleEntity.class)))
            .then(invocation -> {
                final RoleEntity entity = invocation.getArgumentAt(0, RoleEntity.class);
                entity.setId(ROLE_ID_1);
                return entity;
            });

        final NewRoleDto newRole = new NewRoleDto(NAME_ROLE);
        final RoleResource roleResource = service.create(newRole);

        ArgumentCaptor<RoleEntity> roleEntityCaptor = ArgumentCaptor.forClass(RoleEntity.class);
        verify(roleDao).save(roleEntityCaptor.capture());
        assertEquals(roleEntityCaptor.getValue().getName(), NAME_ROLE);

        assertNotNull(roleResource);
        assertEquals(roleResource.getId(), ROLE_ID_1);
        assertEquals(roleResource.getName(), NAME_ROLE);
    }

    @Test
    public void getAll() {
        final List<RoleEntity> collectionRoleEntity = getCollectionRoleEntity();
        when(roleDao.findAll()).thenReturn(collectionRoleEntity);

        final List<RoleResource> roleResources = service.getAll();
        assertEquals(roleResources.size(), collectionRoleEntity.size());
        for (int i = 0; i < collectionRoleEntity.size(); i++) {
            final RoleEntity roleEntity = collectionRoleEntity.get(i);
            final RoleResource roleResource = roleResources.get(i);
            assertEquals(roleEntity.getId(), roleResource.getId());
            assertEquals(roleEntity.getName(), roleResource.getName());
        }
    }

    @Test
    public void getById() {
        final RoleEntity roleEntity = new RoleEntity(ROLE_ID_1, ROLE_NAME_1);
        when(roleDao.findOne(anyLong())).thenReturn(roleEntity);

        final RoleResource roleResource = service.getById(ROLE_ID_1);
        assertNotNull(roleResource);
        assertEquals(roleEntity.getId(), roleResource.getId());
        assertEquals(roleEntity.getName(), roleResource.getName());
    }

    @Test
    public void update() {
        final RoleEntity originalRoleEntity = new RoleEntity(ROLE_ID_1, ROLE_NAME_1);
        when(roleDao.findOne(anyLong())).thenReturn(originalRoleEntity);

        final UpdateRoleDto updateRoleDto = new UpdateRoleDto(NAME_ROLE);
        final RoleResource updatedRoleResource = service.update(ROLE_ID_1, updateRoleDto);
        assertNotNull(updatedRoleResource);
        assertEquals(updatedRoleResource.getId(), originalRoleEntity.getId());
        assertEquals(updatedRoleResource.getName(), NAME_ROLE);
    }

    private List<RoleEntity> getCollectionRoleEntity() {
        final List<RoleEntity> result = new ArrayList<>();
        result.add(new RoleEntity(ROLE_ID_1, ROLE_NAME_1));
        result.add(new RoleEntity(ROLE_ID_2, ROLE_NAME_2));
        return result;
    }
}
