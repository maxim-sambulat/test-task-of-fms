package com.maximsambulat.service.user;

import com.maximsambulat.configuration.UserServiceTestConfiguration;
import com.maximsambulat.dao.RoleDao;
import com.maximsambulat.dao.UserDao;
import com.maximsambulat.domain.dto.NewUserDto;
import com.maximsambulat.domain.dto.UpdateUserDto;
import com.maximsambulat.domain.entity.RoleEntity;
import com.maximsambulat.domain.entity.UserEntity;
import com.maximsambulat.domain.resource.RoleResource;
import com.maximsambulat.domain.resource.UserResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Maxim Sambulat.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UserServiceTestConfiguration.class)
public class UserServiceTest {
    private final Long USER_ID_1 = 1L;
    private final Long USER_ID_2 = 2L;
    private final Long ROLE_ID_1 = 10L;
    private final Long ROLE_ID_2 = 20L;
    private final boolean USER_IS_ACTIVE_1 = true;
    private final boolean USER_IS_ACTIVE_2 = false;
    private final String USER_NAME_1 = "User 1";
    private final String USER_NAME_2 = "User 2";
    private final String ROLE_NAME_1 = "ADMIN";
    private final String USER_PASSWORD_1 = "Password 1";
    private final String USER_PASSWORD_2 = "Password 2";
    @Autowired
    private UserService service;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;

    @Test
    public void create() {
        when(userDao.save(any(UserEntity.class)))
            .then(invocation -> {
                final UserEntity entity = invocation.getArgumentAt(0, UserEntity.class);
                entity.setId(USER_ID_1);
                return entity;
            });

        final NewUserDto newUser = new NewUserDto(USER_IS_ACTIVE_1, USER_NAME_1, USER_PASSWORD_1);
        final UserResource userResource = service.create(newUser);

        ArgumentCaptor<UserEntity> userEntityCaptor = ArgumentCaptor.forClass(UserEntity.class);
        verify(userDao).save(userEntityCaptor.capture());
        final UserEntity captorValue = userEntityCaptor.getValue();
        assertEquals(captorValue.isActive(), USER_IS_ACTIVE_1);
        assertEquals(captorValue.getUsername(), USER_NAME_1);
        assertEquals(captorValue.getPassword(), USER_PASSWORD_1);

        assertNotNull(userResource);
        assertEquals(userResource.getId(), USER_ID_1);
        assertEquals(userResource.isActive(), USER_IS_ACTIVE_1);
        assertEquals(userResource.getUsername(), USER_NAME_1);
        assertEquals(userResource.getPassword(), USER_PASSWORD_1);
    }

    @Test
    public void getAll() {
        final List<UserEntity> collectionUserEntity = getCollectionUserEntity();
        when(userDao.findAll()).thenReturn(collectionUserEntity);

        final List<UserResource> userResources = service.getAll();
        assertEquals(userResources.size(), collectionUserEntity.size());
        for (int i = 0; i < collectionUserEntity.size(); i++) {
            final UserEntity userEntity = collectionUserEntity.get(i);
            final UserResource userResource = userResources.get(i);
            assertEquals(userEntity.getId(), userResource.getId());
            assertEquals(userEntity.isActive(), userResource.isActive());
            assertEquals(userEntity.getUsername(), userResource.getUsername());
            assertEquals(userEntity.getPassword(), userResource.getPassword());
        }
    }

    @Test
    public void getById() {
        final UserEntity userEntity = new UserEntity(USER_ID_1, USER_IS_ACTIVE_1, USER_NAME_1, USER_PASSWORD_1, null);
        when(userDao.findOne(anyLong())).thenReturn(userEntity);

        final UserResource userResource = service.getById(USER_ID_1);
        assertNotNull(userResource);
        assertEquals(userEntity.getId(), userResource.getId());
        assertEquals(userEntity.isActive(), userResource.isActive());
        assertEquals(userEntity.getUsername(), userResource.getUsername());
        assertEquals(userEntity.getPassword(), userResource.getPassword());
    }

    @Test
    public void update() {
        final UserEntity originalUserEntity =
            new UserEntity(USER_ID_1, USER_IS_ACTIVE_1, USER_NAME_1, USER_PASSWORD_1, null);
        when(userDao.findOne(anyLong())).thenReturn(originalUserEntity);

        final UpdateUserDto updateUserDto = new UpdateUserDto(USER_IS_ACTIVE_2, USER_NAME_2, USER_PASSWORD_2);
        final UserResource updatedUserResource = service.update(USER_ID_1, updateUserDto);
        assertNotNull(updatedUserResource);
        assertEquals(updatedUserResource.getId(), originalUserEntity.getId());
        assertEquals(updatedUserResource.isActive(), USER_IS_ACTIVE_2);
        assertEquals(updatedUserResource.getUsername(), USER_NAME_2);
        assertEquals(updatedUserResource.getPassword(), USER_PASSWORD_2);
    }

    @Test
    public void addRole() {
        final UserEntity userEntity =
            new UserEntity(USER_ID_1, USER_IS_ACTIVE_1, USER_NAME_1, USER_PASSWORD_1, new HashSet<>());
        when(userDao.findOne(USER_ID_1)).thenReturn(userEntity);

        final RoleEntity roleEntity = new RoleEntity(ROLE_ID_1, ROLE_NAME_1);
        when(roleDao.findOne(ROLE_ID_1)).thenReturn(roleEntity);

        service.addRole(USER_ID_1, ROLE_ID_1);
        assertEquals(userEntity.getRoles().size(), 1);
    }

    @Test
    public void addRoleExist() {
        final RoleEntity roleEntity = new RoleEntity(ROLE_ID_1, ROLE_NAME_1);
        when(roleDao.findOne(ROLE_ID_1)).thenReturn(roleEntity);

        final Set<RoleEntity> roles = new HashSet<>();
        roles.add(roleEntity);
        final UserEntity userEntity =
            new UserEntity(USER_ID_1, USER_IS_ACTIVE_1, USER_NAME_1, USER_PASSWORD_1, roles);
        when(userDao.findOne(USER_ID_1)).thenReturn(userEntity);

        service.addRole(USER_ID_1, ROLE_ID_1);
        assertEquals(userEntity.getRoles().size(), 1);
    }

    @Test
    public void getAllRoles() {
        final UserEntity userEntityEmptyRoles =
            new UserEntity(USER_ID_1, USER_IS_ACTIVE_1, USER_NAME_1, USER_PASSWORD_1, new HashSet<>());

        final Set<RoleEntity> roles = new HashSet<>();
        roles.add(new RoleEntity(ROLE_ID_1, ROLE_NAME_1));
        final UserEntity userEntity =
            new UserEntity(USER_ID_2, USER_IS_ACTIVE_1, USER_NAME_1, USER_PASSWORD_1, roles);

        when(userDao.findOne(USER_ID_1)).thenReturn(userEntityEmptyRoles);
        when(userDao.findOne(USER_ID_2)).thenReturn(userEntity);

        List<RoleResource> emptyRoles = service.getAllRoles(USER_ID_1);
        assertEquals(emptyRoles.size(), 0);

        List<RoleResource> notEmptyRoles = service.getAllRoles(USER_ID_2);
        assertEquals(notEmptyRoles.size(), 1);
    }

    @Test
    public void deleteRole() {
        final Set<RoleEntity> roles = new HashSet<>();
        roles.add(new RoleEntity(ROLE_ID_1, ROLE_NAME_1));
        final UserEntity userEntity =
            new UserEntity(USER_ID_2, USER_IS_ACTIVE_1, USER_NAME_1, USER_PASSWORD_1, roles);
        when(userDao.findOne(USER_ID_1)).thenReturn(userEntity);

        service.deleteRole(USER_ID_1, ROLE_ID_2);
        assertEquals(userEntity.getRoles().size(), 1);

        service.deleteRole(USER_ID_1, ROLE_ID_1);
        assertEquals(userEntity.getRoles().size(), 0);
    }

    private List<UserEntity> getCollectionUserEntity() {
        final List<UserEntity> result = new ArrayList<>();
        result.add(new UserEntity(USER_ID_1, USER_IS_ACTIVE_1, USER_NAME_1, USER_PASSWORD_1, null));
        result.add(new UserEntity(USER_ID_2, USER_IS_ACTIVE_2, USER_NAME_2, USER_PASSWORD_2, null));
        return result;
    }
}
